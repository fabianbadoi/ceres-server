TARGET = tests

QT -= gui

CONFIG += console
CONFIG += c++1z
CONFIG -= app_bundle

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

INCLUDEPATH += /usr/include/c++/7
INCLUDEPATH += ../vendor
INCLUDEPATH += ../src

LIBS += ../src/headers.o \
        ../src/request.o \
        ../src/exception.o \
        ../src/response.o \
        ../src/httpResponder.o \
        ../src/string.o \
        ../src/stringCapturingOutput.o

SOURCES += \
    main.cpp \
    http2/http/headers.cpp \
    http2/http/responders/httpResponder.cpp \
    http2/utility/string/get_line.cpp \
    http2/utility/string/trim.cpp \
    http2/utility/stringCapturingOutput.cpp

QMAKE_CXX = /usr/bin/clang-5.0
QMAKE_CXXFLAGS += -Wall -Wextra -Wpedantic -Weffc++
