#include <catch.hpp>

#include "../src/http2/utility/stringCapturingOutput.hpp"
#include <string>
#include <vector>

using namespace http2::utility;
using std::string;
using std::vector;

SCENARIO("StringCapturingOutput can be used to sniff output")
{
    GIVEN("a new capturer")
    {
        StringCapturingOutput capturer;

        WHEN("data is written to it")
        {
            string data = "this is some data";
            vector<uint8_t> data_as_buffer(data.begin(), data.end());

            capturer.write(data_as_buffer);

            THEN("the data can taken from it")
            {
                CHECK(data == capturer.take_data());
            }
        }
    }

    GIVEN("a capturer with some data to in it")
    {
        StringCapturingOutput capturer;

        string data = "this is some data";
        vector<uint8_t> data_as_buffer(data.begin(), data.end());
        capturer.write(data_as_buffer);

        WHEN("data is taken out")
        {
            capturer.take_data();

            THEN("the capturer is emptied")
            {
                CHECK("" == capturer.take_data());
            }
        }
    }
}
