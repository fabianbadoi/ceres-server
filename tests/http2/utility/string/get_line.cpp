#include <catch.hpp>

#include "../src/http2/utility/string.hpp"

using http2::utility::get_lines;
using std::string;

SCENARIO("splitting strings", "[utility]")
{
    GIVEN("an empty string")
    {
        string empty = "";

        WHEN("we try to split it")
        {
            auto lines = get_lines(empty);

            THEN("we get an empty result")
            {
                CHECK(0 == lines.size());
            }
        }
    }

    GIVEN("a string with only one line")
    {
        string one_line = "hello, world!";

        WHEN("we try to split it")
        {
            auto lines = get_lines(one_line);

            THEN("we get only one line")
            {
                CHECK(1 == lines.size());
            }
        }
    }

    GIVEN("a string with multiple lines")
    {
        string some_lines = "hello\r\nis it me\r\n you're looking for";

        WHEN("we try to split it")
        {
            auto lines = get_lines(some_lines);

            THEN("we get the right number of lines")
            {
                CHECK(lines.size() == 3);
            }
        }
    }

    GIVEN("a string with consecutive new line delimiters")
    {
        string string_with_consecutive_delimiters = "hello\r\n\r\nWorld\r\n\r\n";

        WHEN("we try to split it") {
            auto lines = get_lines(string_with_consecutive_delimiters);

            THEN("we get emply lines for them")
            {
                CHECK(lines.size() == 4);
                CHECK(lines[1] == "");
                CHECK(lines[3] == "");
            }
        }
    }
}