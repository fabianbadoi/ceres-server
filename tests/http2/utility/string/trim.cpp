#include <catch.hpp>

#include "../src/http2/utility/string.hpp"

using http2::utility::trim;
using std::string;

SCENARIO("trimming strings", "[utility]")
{
    GIVEN("an empty string")
    {
        auto trimmed = trim("");

        THEN("we get an empty string")
        {
            CHECK(trimmed == "");
        }
    }

    GIVEN("a string that doesn't need trimming")
    {
        string input = "this is not trimmable";
        auto output = trim(input);

        THEN("we get the same string back")
        {
            CHECK(input == output);
        }
    }

    GIVEN("a string with whitespace at the start")
    {
        auto trimmed = trim(" hello, world!");

        THEN("we get the end back")
        {
            CHECK(trimmed == "hello, world!");
        }
    }

    GIVEN("a stirng with whitespace at the end")
    {
        auto trimmed = trim("hello, world! ");

        THEN("we get the beginning back")
        {
            CHECK(trimmed == "hello, world!");
        }
    }

    GIVEN("a string with whitespace at both ends")
    {
        auto trimmed = trim(" hello ");
        
        THEN("we get the core back")
        {
            CHECK(trimmed == "hello");
        }
    }

    GIVEN("a string with multiple whitespace around its core")
    {
        auto trimmed = trim("    hello          ");

        THEN("we get the core back")
        {
            CHECK(trimmed == "hello");
        }
    }

    GIVEN("a string with non-space white space chars around it")
    {
        auto trimmed = trim("\t\n hello\r");

        THEN("we get the core back")
        {
            CHECK(trimmed == "hello");
        }
    }
}