#include <catch.hpp>

#include "../src/http2/exception.hpp"
#include "../src/http2/http/headers.hpp"

using namespace http2::http;
using http2::Exception;

SCENARIO("headers can be manipulated", "[http:header]")
{
    GIVEN("empty headers")
    {
        Headers headers;

        CHECK(headers.size() == 0);

        WHEN("a new header is added")
        {
            headers.add_new("key", "value");

            THEN("the size of the header increases by 1")
            {
                CHECK(headers.size() == 1);
            }
            AND_THEN("the header has that item")
            {
                CHECK(headers.has("key"));
            }
            AND_THEN("the item is available")
            {
                CHECK(*headers.get("key") == "value");
            }
        }
    }

    GIVEN("a header that already has items")
    {
        Headers headers{ { "k1", "v1" }, { "k2", "v2" }, { "k3", "v3" } };

        REQUIRE(headers.size() == 3);

        WHEN("an existing header is added as if it were new")
        {
            THEN("an exception is thrown")
            {
                CHECK_THROWS_AS(headers.add_new("k1", "v11"), Exception);
            }
        }

        WHEN("setting an header that already exists")
        {
            headers.set("k1", "v11");

            THEN("the operation succeeds")
            {
                CHECK(*headers.get("k1") == "v11");
            }
        }

        WHEN("an item is removed")
        {
            headers.remove("k1");

            THEN("the item is not longer available")
            {
                CHECK(!headers.has("k1"));
            }
        }
    }
}
