#include <catch.hpp>

#include <sstream>
#include <string>

#include "../src/http2/exception.hpp"
#include "../src/http2/http/common.hpp"
#include "../src/http2/http/headers.hpp"
#include "../src/http2/http/responders/httpResponder.hpp"
#include "../src/http2/http/response.hpp"
#include "../src/http2/utility/stringCapturingOutput.hpp"
#include "../src/http2/utility/string.hpp"

using namespace http2::http;
using http2::http::HttpStatusCode;
using http2::http::responders::HttpResponder;
using http2::utility::StringCapturingOutput;
using http2::utility::get_lines;

Response make_response(HttpStatusCode, std::initializer_list<HeaderItem>, std::string);

SCENARIO("producing http responses", "[http:response]")
{
    GIVEN("a response")
    {
        auto response = make_response(
            HttpStatusCode::NotFound,
            { { "header1", "v1" }, { "header2", "v2" } },
            "response body\non multiple lines");

        WHEN("it's used with the http responder")
        {
            StringCapturingOutput capture;
            HttpResponder responder;

            responder.respond(capture, response);
            auto output = capture.take_data();
            auto lines_of_response = get_lines(output);

            REQUIRE(lines_of_response.size() >= 5);

            THEN("the response has a status code and message")
            {
                CHECK("HTTP/1.1 404 Not Found" == lines_of_response[0]);
            }
            AND_THEN("the response contains the right headers")
            {
                CHECK("header1: v1" == lines_of_response[1]);
                CHECK("header2: v2" == lines_of_response[2]);
            }
            AND_THEN("there's a new line after the headers")
            {
                CHECK("" == lines_of_response[3]);
            }
            AND_THEN("the response body is at the end of the stream")
            {
                CHECK("response body\non multiple lines" == lines_of_response[4]);
            }
        }
    }
}

Response make_response(HttpStatusCode status_code, std::initializer_list<HeaderItem> headers, std::string body)
{
    Body body_as_byte_vector(body.begin(), body.end());

    Response response(status_code, Headers(headers), std::forward<Body>(body_as_byte_vector));
    return std::forward<Response>(response);
}
