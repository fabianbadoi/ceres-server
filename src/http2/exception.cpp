#include "exception.hpp"

using http2::Exception;

Exception::Exception(const char* message)
    : std::runtime_error(message)
{
}