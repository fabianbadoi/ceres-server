#pragma once

#include <optional>
#include <string>

#include "common.hpp"
#include "headers.hpp"

namespace http2::http {

class Request {
public:
    Request(HttpMethod&&, std::string&&, Headers&&, Body&&);
    HttpMethod get_method();
    const std::string& get_uri();
    const Headers& get_headers();
    const Body& get_body();

private:
    HttpMethod method;
    std::string uri;
    Headers headers;
    Body body;
};
}
