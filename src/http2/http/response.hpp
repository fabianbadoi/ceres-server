#pragma once

#include "common.hpp"
#include "headers.hpp"

namespace http2::http {

class Response {
public:
    Response(HttpStatusCode, Headers&&, Body&&);

    HttpStatusCode get_status_code();
    Headers& get_headers();
    Body& get_body();

private:
    HttpStatusCode status_code;
    Headers headers;
    Body body;
};
}
