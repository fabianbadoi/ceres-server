#pragma once

#include <initializer_list>
#include <map>
#include <string>

namespace http2::http {

using HeaderMap = std::map<std::string, std::string>;
using HeaderItem = std::pair<const std::string, std::string>;

class Headers {
public:
    Headers() = default;
    Headers(std::initializer_list<HeaderItem>);

    size_t size();
    bool has(const std::string& name);
    void add_new(const std::string& name, const std::string& value);
    void set(const std::string& name, const std::string& value);
    void remove(const std::string& name);
    const std::string* get(const std::string& name);

    HeaderMap::const_iterator begin() const noexcept;
    HeaderMap::const_iterator end() const noexcept;

private:
    std::map<std::string, std::string> headers;
};
}
