#pragma once

#include "../common.hpp"
#include "../request.hpp"

namespace http2::http::requestParsers {
using http2::http::Body;
using http2::http::Request;

class IRequestParser {
public:
    IRequestParser() = default;

    IRequestParser(const IRequestParser&) = default;
    IRequestParser& operator=(const IRequestParser&) = delete;
    IRequestParser(const IRequestParser&&) = delete;
    IRequestParser& operator=(const IRequestParser&&) = delete;

    virtual ~IRequestParser() = default;
    virtual Request parse(const Body& body) = 0;
};
}
