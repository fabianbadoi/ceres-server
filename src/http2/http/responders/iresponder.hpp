#pragma once

#include "../../utility/ioutput.hpp"
#include "../response.hpp"

using http2::utility::IOutput;

namespace http2::http::responders {

class IResponder {
public:
    IResponder() = default;

    IResponder(const IResponder&) = default;
    IResponder& operator=(const IResponder&) = delete;
    IResponder(const IResponder&&) = delete;
    IResponder& operator=(const IResponder&&) = delete;

    virtual ~IResponder() = default;
    virtual void respond(IOutput&, Response&) = 0;
};
}
