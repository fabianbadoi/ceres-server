#pragma once

#include "iresponder.hpp"

namespace http2::http::responders {

class HttpResponder : public IResponder {
public:
    HttpResponder() = default;

    HttpResponder(const HttpResponder&) = delete;
    HttpResponder(const HttpResponder&&) = delete;
    HttpMethod& operator=(const HttpResponder&) = delete;
    HttpMethod& operator=(const HttpResponder&&) = delete;

    ~HttpResponder() override = default;
    void respond(IOutput&, Response&) override;

private:
    void write_status(IOutput& output, HttpStatusCode status_code);
    void write_headers(IOutput& output, const Headers& headers);
    void write_header_body_separator(IOutput& output);
    void write_body(IOutput& output, const Body& body);

    void write_line(IOutput& output, const std::string& line);
    http2::http::Body str_to_bytes(const std::string& str);
};
}
