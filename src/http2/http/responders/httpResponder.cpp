#include "httpResponder.hpp"

#include <map>

using http2::http::Response;
using http2::http::Headers;
using http2::http::Body;
using http2::http::responders::HttpResponder;
using http2::utility::IOutput;

void HttpResponder::respond(IOutput& output_stream, Response& response)
{
    write_status(output_stream, response.get_status_code());
    write_headers(output_stream, response.get_headers());
    write_header_body_separator(output_stream);
    write_body(output_stream, response.get_body());
}

void HttpResponder::write_status(IOutput& output, HttpStatusCode status_code)
{
    std::map<HttpStatusCode, std::string> status_map = {
        {HttpStatusCode::OK, "OK"},
        {HttpStatusCode::Redirect, "Redirected"},
        {HttpStatusCode::ClientError, "Client Error"},
        {HttpStatusCode::NotFound, "Not Found"},
        {HttpStatusCode::ServerError, "Server Error"}
    };

    auto line = "HTTP/1.1 " + std::to_string(status_code) + " " + status_map.at(status_code);

    write_line(output, line);
}

void HttpResponder::write_headers(IOutput& output, const Headers& headers)
{
    for (auto [name, value] : headers) {
        write_line(output, name + ": " + value); // TODO escape
    }
}

void HttpResponder::write_header_body_separator(IOutput& output)
{
    write_line(output, "");
}

void HttpResponder::write_body(IOutput& output, const Body& body)
{
    output.write(body);
}

void HttpResponder::write_line(IOutput& output, const std::string& line)
{
    output.write(str_to_bytes(line));
    output.write(str_to_bytes("\r\n")); // TODO refactor \r\n into HTTP_NEWLINE;
}

http2::http::Body HttpResponder::str_to_bytes(const std::string& str)
{
    Body data(str.begin(), str.end());

    return std::forward<Body>(data);
}
