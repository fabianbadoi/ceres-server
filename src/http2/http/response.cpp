#include "response.hpp"

using http2::http::Body;
using http2::http::Headers;
using http2::http::HttpStatusCode;
using http2::http::Response;

Response::Response(HttpStatusCode status_code, Headers&& headers, Body&& body)
    : status_code(status_code)
    , headers(headers)
    , body(body)
{
}

HttpStatusCode Response::get_status_code()
{
    return status_code;
}

Headers& Response::get_headers()
{
    return headers;
}

Body& Response::get_body()
{
    return body;
}
