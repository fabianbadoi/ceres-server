#pragma once

#include <cstdint>
#include <vector>

namespace http2::http {

using Body = std::vector<uint8_t>;

enum class HttpMethod {
    Get,
    Post,
    Put,
    Delete,
    Head,
};

enum HttpStatusCode {
    OK = 200,
    Redirect = 300,
    ClientError = 400,
    NotFound = 404,
    ServerError = 500,
};
}
