#include "request.hpp"

using http2::http::Body;
using http2::http::Headers;
using http2::http::HttpMethod;
using http2::http::Request;

Request::Request(
    HttpMethod&& method,
    std::string&& uri,
    Headers&& headers,
    Body&& body)
    : method(method)
    , uri(uri)
    , headers(headers)
    , body(body)
{
}

HttpMethod Request::get_method()
{
    return method;
}

const std::string& Request::get_uri()
{
    return uri;
}

const Headers& Request::get_headers()
{
    return headers;
}

const Body& Request::get_body()
{
    return body;
}
