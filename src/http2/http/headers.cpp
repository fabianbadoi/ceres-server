#include "headers.hpp"

#include <memory>

#include "../exception.hpp"

using http2::Exception;
using http2::http::HeaderItem;
using http2::http::HeaderMap;
using http2::http::Headers;
using std::string;

Headers::Headers(std::initializer_list<HeaderItem> some_headers)
{
    headers.insert(some_headers);
}

size_t Headers::size()
{
    return headers.size();
}

bool Headers::has(const string& name)
{
    return headers.find(name) != headers.end();
}

void Headers::add_new(const string& name, const string& value)
{
    if (has(name)) {
        throw Exception(string("Header value already exists: " + name).c_str());
    }

    set(name, value);
}

const string* Headers::get(const string& name)
{
    return has(name) ? &headers[name] : nullptr;
}

void Headers::set(const string& name, const string& value)
{
    headers[string(name)] = string(value);
}

void Headers::remove(const string& name)
{
    headers.erase(name);
}


HeaderMap::const_iterator Headers::begin() const noexcept
{
    return headers.begin();
}

HeaderMap::const_iterator Headers::end() const noexcept
{
    return headers.end();
}
