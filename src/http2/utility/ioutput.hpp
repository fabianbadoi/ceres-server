#pragma once

#include <cstdint>
#include <vector>

namespace http2::utility {

using Data = std::vector<uint8_t>;

class IOutput {
public:
    IOutput() = default;
    IOutput(const IOutput&) = delete;
    IOutput& operator=(const IOutput&) = delete;
    IOutput(const IOutput&&) = delete;
    IOutput& operator=(const IOutput&&) = delete;

    virtual ~IOutput() = default;
    virtual void write(const Data&) = 0;
};
}