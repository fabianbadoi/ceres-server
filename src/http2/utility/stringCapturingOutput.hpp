#pragma once

#include "ioutput.hpp"
#include <sstream>
#include <string>

namespace http2::utility {

class StringCapturingOutput : public IOutput {
public:
    ~StringCapturingOutput() override = default;
    void write(const Data&) override;

    StringCapturingOutput() = default;

    StringCapturingOutput(const StringCapturingOutput&) = delete;
    StringCapturingOutput(const StringCapturingOutput&&) = delete;
    StringCapturingOutput& operator=(const StringCapturingOutput&) = delete;
    StringCapturingOutput& operator=(const StringCapturingOutput&&) = delete;

    std::string take_data();

private:
    std::stringstream buffer;
};
}