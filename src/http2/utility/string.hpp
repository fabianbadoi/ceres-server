#include <string>
#include <vector>

namespace http2::utility {
std::vector<std::string_view> get_lines(const std::string& input);
std::string trim(const std::string_view& input);
}
