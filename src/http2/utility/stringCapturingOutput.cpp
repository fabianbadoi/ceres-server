#include "stringCapturingOutput.hpp"

#include "ioutput.hpp"
#include <utility>

using http2::utility::StringCapturingOutput;
using std::string;

void StringCapturingOutput::write(const Data& data)
{
    buffer << string(data.begin(), data.end());
}

string StringCapturingOutput::take_data()
{
    auto data_so_far = buffer.str();

    buffer.str("");

    return std::forward<string>(data_so_far);
}
