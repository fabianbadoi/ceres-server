#include "string.hpp"

#include <algorithm>

using std::min;
using std::string;
using std::string_view;
using std::vector;

vector<string_view> http2::utility::get_lines(const string& input)
{
    vector<string_view> lines;

    string delimiter = "\r\n";
    int delimiter_length = delimiter.length();
    int next_delimiter_position = 0;

    for (
        auto rest_of_input = string_view(input);
        rest_of_input.length() > 0;
        rest_of_input = rest_of_input.substr(
            min(next_delimiter_position + delimiter_length, (int) rest_of_input.length())
        )
    ) {
        next_delimiter_position = rest_of_input.find(delimiter);

        if (next_delimiter_position < 0) {
            next_delimiter_position = rest_of_input.length();
        }

        auto one_line = rest_of_input.substr(0, next_delimiter_position);
        lines.push_back(one_line);
    }

    return lines;
}

string http2::utility::trim(const string_view& input)
{
    const string any_newline_char = " \r\t\n";

    auto first_non_space = input.find_first_not_of(any_newline_char);
    auto trimmed_start = first_non_space == string::npos ?
        input : input.substr(first_non_space);

    auto last_non_space = trimmed_start.find_last_not_of(any_newline_char);
    auto trimmed_both = last_non_space == string::npos ?
        trimmed_start : trimmed_start.substr(0, last_non_space + 1);

    return string(trimmed_both);
}
