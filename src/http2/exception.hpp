#pragma once

#include <stdexcept>

namespace http2 {

class Exception : public std::runtime_error {
public:
    explicit Exception(const char* message);
};
}
