TARGET = http2

QT -= gui
QT += network

CONFIG += console
CONFIG += c++1z
CONFIG -= app_bundle

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

INCLUDEPATH += /usr/include/c++/7
INCLUDEPATH += ../vendor

SOURCES += \
    main.cpp \
    http2/exception.cpp \
    http2/utility/string.cpp \
    http2/utility/stringCapturingOutput.cpp \
    http2/http/request.cpp \
    http2/http/response.cpp \
    http2/http/headers.cpp \
    http2/http/responders/httpResponder.cpp

QMAKE_CXX = /usr/bin/clang-5.0
QMAKE_CXXFLAGS += -Wall -Wextra -Wpedantic -Weffc++

HEADERS += \
    http2/exception.hpp \
    http2/http/request.hpp \
    http2/http/headers.hpp
