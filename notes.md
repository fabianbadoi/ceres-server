Server Description
==================

- listens in a thread
- on new connection

    - moves the socket to a new thread, all future actions are on that thread
    - waits for data on the socket
    - creates a request object
    - passes the request object to the request handler
    - gets response object from the request handler
    - passes the response to the responder
